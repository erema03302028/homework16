﻿#include <iostream>
#include <cmath>


class Vector
{
public:
	Vector() : x(5), y(-8), z(7)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
		std::cout << x << ' ' << y << ' ' << z<<'\n';
	}
	void Module() //Вычисление модуля длины вектора
	{
		std::cout <<"Lenght vector: "<< sqrt(x * x + y * y + z * z);
	}
private:
	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
	Vector v;
	v.Show();
	v.Module();
}